This is my main public Git account.

I also use [GitHub](https://github.com/Exagone313) for contributions, and my [self-hosted GitLab instance](https://gitlab.ewd.app) for more private projects.

Join me now on [Mastodon](https://share.elouworld.org/@Exagone313)!
